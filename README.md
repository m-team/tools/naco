[![PyPI Badge](https://img.shields.io/pypi/v/naco.svg)](https://pypi.python.org/pypi/naco)
[![Read the Docs](https://readthedocs.org/projects/naco/badge/?version=latest)](https://naco.readthedocs.io/en/latest/?version=latest)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

# NFDI Attribute Checker OIDC
Check Attributes for compliance with NFDI expectations

## Installation
NFDI Attribute Checker OIDC is available on [PyPI](https://pypi.org/project/naco/). Install using `pip`:
```
pip install naco
```

You can also install from the git repository:
```
git clone https://github.com/marcvs/naco
pip install -e ./naco
```
