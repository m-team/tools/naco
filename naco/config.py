# vim: tw=100 foldmethod=indent

import logging
import os
import sys
from pathlib import Path
from configparser import ConfigParser
from configparser import ExtendedInterpolation
from naco.parse_args import args

logger = logging.getLogger(__name__)


class MyConfigParser(ConfigParser):
    def getlist(self, section, option, fallback=None):
        if not fallback:
            value = self.get(section, option)
        else:
            value = self.get(section, option, fallback=fallback)
        lines = list(filter(None, (x.strip() for x in value.splitlines()))) 
        rv=[]
        for l in lines:
            #  logger.info(F"line: {l}")
            for e in l.split(','):
                f=e.rstrip(' ').lstrip(' ')
                rv.append(f)
        return rv


CONFIG = MyConfigParser(interpolation=ExtendedInterpolation())

def set_defaults():
    """set argparse defaults from configfile, if those are None"""
    if args.loglevel is None:
        args.loglevel = CONFIG.get("main", "loglevel", fallback = "INFO")
        if args.verbose:
            print(F"default loglevel is set to {args.loglevel}")
    if args.logfile is None:
        args.logfile = CONFIG.get("main", "logfile", fallback = None)
        if args.verbose:
            print(F"default logfile is set to {args.logfile}")

def set_defaults_2():
    """Set defaults in CONFIG from argparse"""
    CONFIG.read_dict(
        {
            "main": {
                "config": args.config,
                "loglevel": args.loglevel,
                "verbose": args.verbose,
                "debug": args.debug,
            }
        }
    )


def load_config():
    """Reload configuration from disk.

    Config locations, by priority (first one wins)
    """
    files = []

    # basename = os.path.basename(sys.argv[0]).rstrip('.py')
    basename = "naco"
    dirname  = os.path.dirname(__file__)
    home = os.getenv("HOME")

    try:
        files += [Path(args.config)]
    except FileNotFoundError:
        pass
    except AttributeError:
        pass
    except TypeError:
        pass

    files += [
        Path(f"/etc/{basename}.conf"),
        Path(f"{home}/.config/{basename}.conf"),
        Path(f"./{basename}.conf"),
        Path(f"{dirname}/{basename}.conf"),
    ]

    read_a_config = False
    for f in files:
        #  print(F"trying config: {f}")
        try:
            if f.exists():
                logger.info("Using this config file: {}".format(f))
                # if args.verbose:
                #     print("Using this config file: {}".format(f))
                CONFIG.read(f)
                read_a_config = True
                break
        except PermissionError:
            pass
    if not read_a_config:
        filelist = [str(f) for f in files]
        filestring = "\n    ".join(filelist)
        logger.warning(
            f"Warning: Could not read any config file from \n" f"    {filestring}"
        )
        # sys.exit(4)


def test_config():
    try:
        _ = CONFIG["main"]["loglevel"]
        _ = CONFIG["main"]["verbose"]
    except KeyError as e:
        logging.error(f"Cannot find required config entry: {e}")
        sys.exit(3)


try:
    load_config()
    set_defaults()
    set_defaults_2()
    test_config()
except AttributeError: # bcs gunicorn or pytest
    pass
