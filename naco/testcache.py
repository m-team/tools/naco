from cache3 import DiskCache


cache = DiskCache(directory=".", name="cache.sqlite3", timeout=100)


myobject={"key": "value", "anotherkey": {"a complex": "value"}}
#  myobject="value"


#  cache.set("a_test", myobject, timeout=None, tag='jesses')


print(F"keys:")
for key in cache.keys(tag='jesses'):
    print(F"------------------------  key: {key}")
    obj=cache.get(key, tag='jesses')
    print(F"TTL: {cache.ttl(key, 'jesses')}")
    #  print_json(obj)


print(F"{'Product':14s}|       TTL")
print("--------------+-----------")
for key in cache.keys(tag='jesses'):
    obj=cache.get(key, tag='jesses')
    print(F"{key:14s}|{cache.ttl(key, 'jesses')}")

print(F"TTL: {cache.ttl('a_test', tag='jesses')}")





