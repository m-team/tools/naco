"""json tools for naco."""
# pylint: disable = logging-fstring-interpolation,

import json
import os
import errno
import re
import time
from datetime import datetime

from naco.logsetup import logger
from naco.config import CONFIG

def format_json(content):
    ## pretty print json
    return json.dumps(
        content,
        sort_keys=True,
        indent=4,
        separators=(",", ": "),
    )

def print_json(content):
    print(format_json(content))

def load_json(filename):
    """load json from file"""
    with open(filename, "rt") as json_file:
        json_data = json.load(json_file)
    return json_data

def save_json(filename, json_data):
    logger.debug(F"Storing json to: {filename}")
    with open(filename, "wt") as json_file:
        json_string = format_json(json_data)
        json_file.write(json_string)

def save_json_in_cache(cache, OP_name, json_data, cache_ttl=-1):
    """save json to cache"""
    ## Add metadata
    date_str = datetime.today().strftime("%Y-%m-%d %H:%M")
    json_data["last update"] = date_str

    json_data["last update epoch"] = epoch_time = int(time.time())

    if cache_ttl == 0:
        cache_ttl=None
    if cache_ttl == -1:
        cache_ttl=CONFIG.getint("cache", "ttl", fallback=120)
    cache.set(F"{OP_name}", json_data, timeout=cache_ttl, tag="raw_json")
    logger.debug(F"stored jaw json for {OP_name} in cache")
    # logger.debug(F"{json_data=}")

def load_json_from_cache(cache, OP_name):
    """load json from cache"""
    raw_json=cache.get(OP_name, tag="raw_json")
    return raw_json

def get_json_cache_inventory(cache):
    """Get list of verdicts from cache"""
    OP_names = cache.keys(tag="raw_json")
    retval={}
    for OP_name in OP_names:
        logger.debug(F"{OP_name=}")
        try:
            retval[OP_name]=cache.get(OP_name,
                                  tag="raw_json")["user_info"]["iss"]
        except KeyError:
            retval[OP_name]=None

    return retval

def load_verdict(filename):
    return load_json(filename)

def ensure_dir_exists(dirname):
    if not os.path.isdir(dirname):
        os.mkdir(dirname)
        logger.info(F"created dir: {dirname}")

def friendly_filename(filename):
    return re.sub(r'[^\w_. -]', '_', filename)
    #  return filename.replace('#','').replace(' ', '_')

def save_verdict(cache, OP_name, json_data, cache_ttl=-1):
    """save verdict to cache"""
    ## Add metadata
    date_str = datetime.today().strftime("%Y-%m-%d %H:%M")
    json_data["last update"] = date_str

    if cache_ttl == 0:
        cache_ttl=None
    if cache_ttl == -1:
        cache_ttl=CONFIG.getint("cache", "ttl", fallback=120)
    cache.set(F"{OP_name}", json_data, timeout=cache_ttl, tag="verdict")
    logger.debug(F"stored {OP_name} in cache")

def get_all_verdicts(cache):
    """Get list of verdicts from cache"""
    cached_entries=cache.keys(tag="verdict")
    verdicts={}
    for entry in cached_entries:
        OP_name = entry
        entry_ttl = cache.ttl(OP_name, tag="verdict")
        logger.debug(F"found entry: {OP_name} ({entry_ttl})")
        verdicts[OP_name] = cache.get(OP_name, tag="verdict")
    return verdicts

def dump_info_json_to_var_tmp(oidc_agent_config, data):
    # make dir if not exists

    path="/var/tmp/naco"
    filename=f"{path}/{oidc_agent_config}"

    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

    # rm outputfile if exists
    if os.path.exists(filename):
        os.remove(filename)

    # write formatted json to output
    f = open(filename, 'w')
    f.write(data)
    f.close

