"""Check jsons against specs"""
# pylint: disable = logging-fstring-interpolation,
# vim: foldmethod=indent

import logging

from naco.logsetup import logger
# from naco import logsetup
from naco.config import CONFIG
from naco import jsontools, tokentools

# logger = logging.getLogger(__name__)


def check_scoping(spec_allowed_key, data, oa_conf):
    """Check scoping depending on type"""
    scopedness = spec_allowed_key.get("scopedness")
    key_attr = spec_allowed_key["key_attr"]
    #  logger.info(jsontools.format_json(data))
    #  logger.info(key_attr)
    data_entry = data.get(key_attr)
    if data_entry is None:
        return None
    #  logger.info(data_entry)

    if scopedness == "external":
        return external_scope_checker(data_entry, oa_conf)
    elif scopedness == "community":
        return community_scope_checker(data_entry, oa_conf)


def external_scope_checker(data_entry, oa_conf):
    (external_scopes, community_scopes) = get_scopes(oa_conf)
    allowed_scopes = external_scopes
    forbidden_scopes = community_scopes
    logger.info(f"checking entry for external scopes {data_entry}")
    allowed_scopes_valid = ensure_all_scopes_allowed(data_entry, allowed_scopes, oa_conf, scopedness="external")
    return allowed_scopes_valid

def community_scope_checker(data_entry, oa_conf):
    (external_scopes, community_scopes) = get_scopes(oa_conf)
    allowed_scopes = community_scopes
    forbidden_scopes = external_scopes
    #  logger.info(f"checking entry for community scopes in data{data_entry}")
    forbidden_scopes_valid = ensure_no_scopes_forbidden(data_entry, forbidden_scopes, oa_conf, scopedness="community")
    return forbidden_scopes_valid

def ensure_no_scopes_forbidden(entries, forbidden_scopes, oa_conf, scopedness):
    """Returns true, only if no entry is in one of the forbidden scopes"""
    no_entry_forbidden = True
    if isinstance(entries, str):
        entries = [entries]
    for entry in entries:
        _, scope = entry.split('@')
        #  logger.info(F"scope '{scope}' must not be in '{forbidden_scopes}'??")
        if scope in forbidden_scopes:
            logger.warning(F"  {oa_conf}: {scopedness}-scope '{scope}' is in forbidden scopes {forbidden_scopes}")
            no_entry_forbidden = False
        #  else:
        #      logger.info("  Scope IS NOT inside")
    #  logger.info(F"  -> {no_entry_forbidden}")
    return no_entry_forbidden

def ensure_all_scopes_allowed(entries, allowed_scopes, oa_conf, scopedness):
    """Returns true, only if all entries are in one of the scopes"""
    all_scopes_allowed = True 
    if isinstance(entries, str):
        entries = [entries]
    for entry in entries:
        try:
            _, scope = entry.split('@')
            #  logger.info(F"scope '{scope}' must be in '{allowed_scopes}'??")
            if scope not in allowed_scopes:
                logger.warning(F"  {oa_conf}: {scopedness}-scope '{scope}' is NOT in allowed scopes {allowed_scopes}")
                all_scopes_allowed = False
            #  else:
            #      logger.info("  Scope IS     inside")
        except ValueError:
            logger.error(f"Cannot split: {entry}")
    #  logger.info(F"  -> {all_scopes_allowed}")
    return all_scopes_allowed




def scoped_attr_checker(required_spec, oa_conf, data) -> bool:
    """Check if the scope of my attributes is correct"""
    #  logger.info(jsontools.format_json(data))
    #  logger.info(data.keys())
    for spec_name, spec in required_spec.items():
        #  logger.info("-------------------------")
        #  logger.info(F"Spec name: {spec_name}")
        #  verdict[spec_name]
        #  logger.info(F"Spec       {jsontools.format_json(spec)}")
        for allowed_key_dict in spec["allowed_keys"]:
            try:
                scopedness = allowed_key_dict["scopedness"]
                #  logger.info(F"got scoping_type: {scopedness}")
                #  logger.info(F"in attr: {allowed_key_dict['key_attr']}")
                attr = allowed_key_dict["key_attr"]
                logger.info(f"Data: {data[attr]}")
                for data_entry in data[attr]:
                    scope_valid = check_scoping(oa_conf, scopedness, data_entry, data)

            except KeyError:
                #  logger.info("key error")
                pass
            except TypeError:
                #  logger.info("type error")
                pass
    return True


def get_scopes(oa_conf):
    """Get allowed scopes by oidc-agent-config"""
    return get_scopes_from_config(oa_conf)


def get_scopes_from_config(oa_conf):
    """Get allowed scopes from config"""
    external_scopes = CONFIG.getlist(oa_conf, "external_scopes", fallback=[])
    community_scopes = CONFIG.getlist(oa_conf, "community_scopes", fallback=[])
    logger.info(f"just read scopes for {oa_conf}: {', '.join(external_scopes)}")
    #  logger.info(f"just read scopes for {oa_conf}: {', '.join(community_scopes)}")
    #  for i in community_scopes:
    #      logger.info(F"  ->{i}<-")
    return (external_scopes, community_scopes)


def type_checker(allowed_types, actual_type):
    """Check the type against a string of allowed types"""
    #  logger.debug(f"  checking  type: {allowed_types} in {type(actual_type)}")
    result = False
    for allowed_type in [allowed_types]:
        if allowed_type == "string_or_list":
            if isinstance(actual_type, (str, list)):
                result = True
        elif allowed_type == "str":
            if isinstance(actual_type, str):
                result = True
        elif allowed_type == "list":
            if isinstance(actual_type, list):
                result = True
        elif allowed_type == "string":
            if isinstance(actual_type, str):
                result = True
        elif allowed_type == "dict":
            if isinstance(actual_type, dict):
                result = True
        elif allowed_type == "bool":
            if isinstance(actual_type, bool):
                result = True
    #  logger.debug(f"    found: {result}")
    return result


def list_checker(spec_allowed_key, data):
    key_attr = spec_allowed_key["key_attr"]
    key_types = spec_allowed_key["key_types"]
    attr_name_found = ""
    type_check = False
    found_it = False
    #  logger.debug(f"{key_attr} - {key_types}")
    listentries = len(key_attr)
    for listentry in key_attr:  # must find all entries of list
        #  logger.debug(f"listentry: {listentry}")
        try:
            _ = data[listentry]
            listentries -= 1
        except KeyError as e:
            logger.info(f"      {key_attr} is not present")
            #  raise
        except Exception as e:
            logger.error(f"Uncaught Exception: {e}")
            raise
    if listentries == 0:
        found_it = True
        logger.debug(F"     marking listentries ({'+'.join(key_attr)}) as found")
        attr_name_found = "+".join(key_attr)

        # type check
        for key_type in key_types:
            # make sure all list entries are of expected type
            sub_listentries = len(key_attr)
            sub_res = False
            for sub_listentry in key_attr:
                sub_res = type_checker(key_type, data[sub_listentry])
                if sub_res:
                    sub_listentries -= 1
            if sub_listentries == 0:
                type_check = sub_res
                break
    else:
        # Not found
        pass
    return (attr_name_found, type_check, found_it)

def normal_checker(spec_allowed_key, data):
    key_attr = spec_allowed_key["key_attr"]
    key_types = spec_allowed_key["key_types"]
    attr_name_found = ""
    type_check = False
    found_it=False
    try:
        _ = data[key_attr]
        found_it = True
    except TypeError as e:
        logger.error(e)
        raise
    except KeyError as e:
        logger.debug(f"      {key_attr} is not present")
        #  logger.debug(F"Did not find key {e}")
        pass
    if found_it:
        logger.debug(F"      marking entry '{key_attr}' as found")
        attr_name_found = key_attr

        # type check
        for key_type in key_types:
            res = type_checker(key_type, data[key_attr])
            type_check = res
            if res is True:
                break
        else:
            # not found
            pass
    return (attr_name_found, type_check, found_it)

def is_key_in_spec(spec, data, oa_conf):
    """Check whether the provided key is in the json file"""
    #  logger.debug(f"-----\nrequired_spec: {jsontools.format_json(spec)}")
    verdict = {}
    #  verdict["found"] = []
    #  verdict["type"] = {}
    #  verdict["scope"] = {}
    found_it= False
    for spec_allowed_key in spec["allowed_keys"]:
        #  logger.info(F"-- spec_allowed_key: {spec_allowed_key}")
        key_attr = spec_allowed_key["key_attr"]
        ### LIST (e.g. key_attr is a list (sub, iss))
        if isinstance(key_attr, list):
            (attr_name_found, type_check, found_it) = list_checker(
                spec_allowed_key, data
            )
        else:  # Not a LIST, i.e. a normal attribute
            (attr_name_found, type_check, found_it) = normal_checker(
                spec_allowed_key, data
            )
        if attr_name_found == "":
            #  logging.warning("Found empty attr_name")
            continue
        verdict[attr_name_found]={}
        if found_it:
            verdict[attr_name_found]["found"]=True
            #  verdict["found"].append(attr_name_found)
            #  if verdict["type"] is True:
            verdict[attr_name_found]["type"] = type_check


        ## SCOPE CHECK

        scope_needs_checking = spec_allowed_key.get("scopedness")
        if scope_needs_checking:
            rv = check_scoping(spec_allowed_key, data, oa_conf)
            verdict[attr_name_found]["scope"] = rv
        #      find allowed_scopes
        #      check scopes
    #  logger.info(jsontools.format_json(verdict))
    return verdict


def check_dict(required_spec, provided_data, check_name="", oa_conf=None) -> dict:
    """check a provided dict for fields specified in the required_spec"""
    #  jsontools.print_json(provided_data)
    verdict = {}
    if check_name:
        logger.debug(f"checking {check_name}")
    for spec_name, spec in required_spec.items():
        #  spec_name: Indentifier email , ...
        logger.debug(f"----- spec-name: {spec_name}")
        verdict[spec_name] = {}
        #  verdict[spec_name]["found"] = []
        # test this point against provided json
        verdict[spec_name] = is_key_in_spec(spec, provided_data, oa_conf)

        #  for tested_key in required_spec[spec_name]["allowed_keys"]:
        #  found_global = False
    #  logger.info(jsontools.format_json(verdict))
    return verdict
