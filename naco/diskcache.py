"""json tools for naco."""
# vim:foldmethod=indent
# pylint: disable = logging-fstring-interpolation,

import logging

from cache3 import DiskCache

from naco.logsetup import logger
# from naco import logsetup
from naco.config import CONFIG

# logger = logging.getLogger(__name__)

def get_cache():
    ttl=CONFIG.getint("cache", "ttl", fallback=100)
    folder=CONFIG.get("cache", "folder", fallback="/tmp")
    file=CONFIG.get("cache", "file", fallback="naco-cache.sqlite3")

    return DiskCache(directory=folder, name=file, timeout=ttl)
