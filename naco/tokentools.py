"""token tools for naco."""
# pylint: disable = logging-fstring-interpolation,

import logging
import sys
import json

import libmytoken
import liboidcagent as agent

from flaat.access_tokens import get_access_token_info
from flaat import BaseFlaat
from flaat.flaat_userinfo import UserInfosPrinter
from flaat.exceptions import FlaatUnauthenticated

from naco.logsetup import logger
# from naco import logsetup
from naco.config import CONFIG
from naco.parse_args import args
from naco import jsontools
from naco.exceptions import MyException

# logger = logging.getLogger(__name__)

def get_iss_from_oa_conf_or_token(oa_conf, access_token=None):
    """Get issuer by oidc-agent config"""
    return CONFIG.get(oa_conf, "iss", fallback="")

def get_at_from_mt(mt, oidc_agent_config=""):
    """get an access_token from a mytoken"""
    try:
        at = libmytoken.get_access_token_from_jwt_mytoken(mt)
    except libmytoken.error.MytokenError as e:
        raise MyException(F"{oidc_agent_config}: mytoken: could not obtain a token -- >{e}<", 1)
    return at

def get_at_from_oidc_agent(oidc_agent_config):
    """ get an access token from oidc agent"""
    try:
        access_token = agent.get_access_token(oidc_agent_config)
        logging.info(F"  using oidc-agent for {oidc_agent_config}")
    except agent.liboidcagent.OidcAgentError as e:
        raise MyException(F"{oidc_agent_config}: oidc-agent could not obtain a token for {e}", 1)
    return access_token

def get_all_info_by_oa_conf(oidc_agent_config):
    """get all info (access token, user info, introspection)"""
    # get attributes via oidc-agent / flaat
    try:
        mytoken=CONFIG[oidc_agent_config]["mytoken"]
        logging.info(F"trying mytoken for {oidc_agent_config}")
        access_token = get_at_from_mt(mytoken, oidc_agent_config)
    except KeyError:
        access_token = get_at_from_oidc_agent(oidc_agent_config)
    except MyException:
        access_token = get_at_from_oidc_agent(oidc_agent_config)
    if access_token is None:
        #  logger.warning("access token is none")
        raise MyException("access token is none", 1)
    at_info = get_access_token_info(access_token)
    if at_info is None:
        logging.warning(F"{oidc_agent_config}: Could not obtain any info with access token")

    # get_flaat
    flaat = BaseFlaat()
    #  print(at_info)
    #  logging.info(f"oidc_agent_config: {oidc_agent_config}")
    try:
        flaat.set_client_id(CONFIG[oidc_agent_config]["client_id"])
        flaat.set_client_secret(CONFIG[oidc_agent_config]["client_secret"])
        logger.debug("client_id and client_secret were set from configfile")
    except KeyError:
        pass

    if at_info is not None:
        if hasattr(at_info, "body"):
            flaat.set_trusted_OP_list([at_info.body["iss"]])
            logger.debug(F"Just set trustedOP from AT: {[at_info.body['iss']]}")
    else:
        iss = CONFIG.get(oidc_agent_config, "iss", fallback=None)
        if iss is not None:
            flaat.set_trusted_OP_list([iss])
            logger.debug(F"Just set trustedOP from config: {iss}")

    user_infos = None
    try:
        user_infos = flaat.get_user_infos_from_access_token(access_token)
    except FlaatUnauthenticated as e:
        logger.warning(F"Cant get user_info from '{oidc_agent_config}', since we're unauthenticated")
        logger.warning(e)
        raise MyException(e) from e

    if user_infos is None:
        logger.warning(F"{oidc_agent_config}: Could not obtain any user infos")
        user_infos_json={}
    else:
        user_infos_json = json.loads(user_infos.toJSON())
    if user_infos is None:
        logger.warning(F"{oidc_agent_config}: Error could not obtain any user infos")
    else:
        if user_infos.access_token_info is None:
            logger.warning(F"{oidc_agent_config}: Could not obtain any access token infos")

    logger.debug(F"user_infos_json:\n{jsontools.format_json(user_infos_json)}")
    jsontools.dump_info_json_to_var_tmp(oidc_agent_config, jsontools.format_json(user_infos_json))
    return user_infos_json

