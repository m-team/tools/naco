#!/bin/bash
#date  >> /tmp/log
#echo $0 $@ >> /tmp/log

# use with vim:
# autocmd FileType html,markdown map <leader>ll :w<cr> :!~/bin/md2html.sh % >/tmp/md.html<cr><cr>

SHOW_OUTPUT="Nope"
AUTO_OUTPUT="I don't think so"
INPUT_FILE=$1
MAX_WIDTH=960
STYLE_FILE=`dirname $0`/style_internal_css.html

while [ $# -gt 0 ]; do
    case "$1" in
    -o|--show-output)   SHOW_OUTPUT="Sure"; OUTPUT_FILE=$2;        shift;;
    -i|--input)         INPUT_FILE=$2;                             shift;;
    -a|--auto-output)   AUTO_OUTPUT="Absolutely"; SHOW_OUTPUT="Sure"    ;;
    -w|--width)         MAX_WIDTH=$2;                              shift;;
    -s|--style)         STYLE_FILE=$2;                             shift;;
    esac
    shift
done

export MAX_WIDTH

echo "SHOW_OUTPUT: $SHOW_OUTPUT"
echo "INPUT_FILE:  $INPUT_FILE"
echo "OUTPUT_FILE: $OUTPUT_FILE"
echo "STYLE_FILE:  $STYLE_FILE"

test -e $INPUT_FILE || {
    echo "Input file $INPUT_FILE not found; ERROR"
    exit 1
}
test -e $STYLE_FILE || {
    echo "" > /tmp/md.html
}
test -e $STYLE_FILE && {
    cat $STYLE_FILE | envsubst > /tmp/md.html
    # cat $STYLE_FILE  > /tmp/md.html
}
pandoc -f gfm -t html $INPUT_FILE >> /tmp/md.html
#pandoc -f markdown_github -t html $INPUT_FILE

[ "x$AUTO_OUTPUT" == "xAbsolutely" ] && {
    OUTPUT_FILE=`echo $INPUT_FILE | sed s/\.md/.html/`
}

[ "x$SHOW_OUTPUT" == "xSure" ] && {
    cat /tmp/md.html >$OUTPUT_FILE
}
