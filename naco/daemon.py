"""json tools for naco."""
# pylint: disable = logging-fstring-interpolation,

import logging
import uvicorn
import sys
import json

from fastapi import FastAPI, Request, Response, status
from fastapi.responses import JSONResponse

from naco.logsetup import logger
# import logging
from naco import diskcache, jsontools
from naco.config import CONFIG

# logger = logging.getLogger(__name__)
VERSION=1


app = FastAPI()

@app.post("/analyse")
async def analyse(request: Request, response: Response, secret=None,
        op_name=None, ttl: int=-1):
    try:
        json_data=await request.json()
    except json.decoder.JSONDecodeError:
        response.status_code = status.HTTP_417_EXPECTATION_FAILED
        return 417

    if op_name is None:
        logging.info("no opname provided")
        response.status_code = status.HTTP_404_NOT_FOUND
        return 404
    if secret is None:
        logging.info("no secret provided")
        response.status_code = status.HTTP_404_NOT_FOUND
        return 404
    if json_data is None:
        logging.info("no json_data provided")
        response.status_code = status.HTTP_404_NOT_FOUND
        return 404

    # get valid user/pass combinations
    valid_accounts=[x for x in CONFIG["web"].keys()]
    logging.info(F"validaccounts: {valid_accounts}")
    if op_name in valid_accounts:
        logging.info("Valid account name used")
        required_secret=CONFIG.get("web", op_name, fallback=None)
        logging.info(F"{required_secret} ?= {secret}")
        if secret != required_secret:
            response.status_code = status.HTTP_401_UNAUTHORIZED
            return 401
    else:
        response.status_code = status.HTTP_418_IM_A_TEAPOT
        return 418

    logging.info(F"auth for {op_name} successful")


    # FIXME: This might be more global
    mandatory_info = jsontools.load_json(
        CONFIG["main"]["mandatory_attributes_access_token"]
    )
    optional_info = jsontools.load_json(
        CONFIG["main"]["optional_attributes_access_token"]
    )
    attributes_to_check = { "## Mandatory Attributes": mandatory_info,
            "## Optional Attributes": optional_info }
    #FIXME: until here

    verdict = {}
    verdict[op_name] = {}
    for attr_name in attributes_to_check.keys():
        verdict[op_name][attr_name] = {}
        attrs_to_check = attributes_to_check[attr_name]
        logger.info(F"OPNAME: {op_name}")
        verdict[op_name][attr_name]["SAML"] = spec_checker.check_dict(attrs_to_check, json_data, "", op_name)
    #  print ("VERDICT")
    #  jsontools.print_json(verdict)

    cache = diskcache.get_cache()
    jsontools.save_verdict(cache, op_name, verdict[op_name], ttl)

    # refresh visualisation with updated verdict in cache
    output_tools.refresh_visualisation_from_cache(cache, attributes_to_check)
    return({"message": "ok"})

@app.get("/version")
def version():
    return VERSION

def validate_apikey(apikey):
    valid_apikeys=[x for x in CONFIG["apikeys"].keys() if CONFIG["apikeys"][x]=="active"]
    if apikey not in valid_apikeys:
        return False
    return True

@app.get("/list_entries")
async def list_entries(apikey: str):
    if not validate_apikey(apikey):
        return JSONResponse({"error": "Not Authorised"}, 403)
    cache = diskcache.get_cache()
    entries = jsontools.get_json_cache_inventory(cache)
    logger.debug(F"{entries=}")
    return JSONResponse(entries)

@app.get("/get_entry/{OP_name}")
async def get_from_cache(OP_name: str, apikey: str):
    if not validate_apikey(apikey):
        return JSONResponse({"error": "Not Authorised"}, 403)
    cache = diskcache.get_cache()
    # OP_name="wlcg"
    desired_data = jsontools.load_json_from_cache(cache, OP_name)
    if not desired_data:
        return JSONResponse({"error:": "Not found"}, status_code=404)
    return JSONResponse(desired_data)


# -------------------------------------------------------------------
# Main function -----------------------------------------------------
def main():
    uvicorn.run(app, host="0.0.0.0", port=8000, log_level="debug")

if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover

#  curl -X 'POST'   -d @testweb.json 'http://localhost:8081/analyse?secret=password&jso&op_name=opname'
#   ttl: default -1 => use default from config
#                0  => permanent
#                other => time in seconds
