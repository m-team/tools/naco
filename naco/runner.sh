#!/bin/bash


BASEDIR=$(cd $0; pwd)

export PYTHONPATH=`dirname $BASEDIR`
echo $PYTHONPATH

python3 ./cli.py

scp verdict.html naco@cvs.data.kit.edu:~/public_html/

