"""Render output"""
# vim:foldmethod=indent
# pylint: disable = logging-fstring-interpolation,

import logging
import subprocess

from naco.logsetup import logger
# from naco import logsetup
from naco.config import CONFIG
from naco import jsontools

# logger = logging.getLogger(__name__)


def md2html(output, filename="verdict.md"):
    """use md2html.sh to convert output to webpage"""
    with open(filename, "w") as text_file:
        text_file.write(output)
    output_dir = CONFIG.get("main", "output", fallback="")
    verdict_file = (
        output_dir
        + "/"
        + CONFIG.get("main", "output_filename", fallback="verdict.html")
    )
    md2html_tool = CONFIG.get("md2html", "tool", fallback="md2html.sh")
    md2html_style = CONFIG.get("md2html", "style", fallback="verdict_css.html")
    md2html_width = CONFIG.get("md2html", "width", fallback="1800")
    subprocess.run(
        [
            md2html_tool,
            "-i",
            filename,
            "--width",
            md2html_width,
            "-o",
            verdict_file,
            "--style",
            md2html_style,
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        check=True,
    )

def render_line_of_dashes(keys, width):
    output = ""
    output += f"|{'-':-<{width}s}|"
    for _ in keys:
        output += f"{'-':-<{width}s}|"
    output += f"{'-':-<{width}s}|"
    output += "\n"
    return output

def render_output_header(verdict, width=11, color="", dashes=False):
    """create an MD table header as output"""
    output = ""
    if color:
        output += f"|<span class='{color}'>{'Product':{width}s}|"
    else:
        output += f"|{'Product':{width}s}|"
    for key in verdict.keys():
        if color:
            output += f"<span class='{color}'>{key:{width}s}|"
        else:
            output += f"{key:{width}s}|"
    if color:
        output += f"<span class='{color}'>{'Last Update':{width}s}|"
    else:
        output += f"{'Last Update':{width}s}|"
    output += "\n"
    # line of dashes
    if dashes:
        output += render_line_of_dashes(verdict.keys(), width)
    return output

def render_output(
    verdicts, product, width=11, color_bad=None, color_good="bg_green", last_update=""
):
    """create an MD table as output"""
    output = ""
    summary = {}

    #  jsontools.print_json(verdicts)
    # initialise summary with False
    #  for key in verdicts["Access Token"].keys():
    #  print (F"verdicts stuff: {verdicts.keys()}")
    #  verdicts_keys = list(verdicts.keys())
    #  for key in verdicts[verdicts_keys[0]].keys():  # only the first element of the verdicts list
    #      summary[key] = False
    #      #  print (F"key: {key}")
    try:
        verdict_keys = verdicts[list(verdicts.keys())[0]].keys()
    except IndexError:
        verdict_keys=[]
        logger.error(f"{product}: Cannot get verdic_keys")
    for key in verdict_keys:
        summary[key] = "Missing"
        #  print (F"key: {key}")

    for check, verdict in verdicts.items():
        #  logger.info(f" - {product} - {check}")
        #  jsontools.print_json(verdict)
        product_check = f"<b>{product}</b> ({check})"
        output += f"|{product_check:{width}s}"
        for key, value in verdict.items():
            #  logger.info("------------------------")
            #  logger.info(F"key: {key}")
            #  logger.info(F"  Value: '{jsontools.format_json(value)}'")
            output +="|"
            if not value.items():
                output += "---"
            for name, content in value.items():
                color = ""
                subput = ""
                summary[key] = ""
                # subformat the entry in the cell
                ## type
                if not content["type"]:
                    subput += " (wrong type)"
                    color = F"<span class='{color_bad}'>"
                    if summary[key] != "":
                        summary[key] += "+ "
                    summary[key] += "Wrong Type "
                ## scope
                try:
                    sco = content["scope"] # True, False or unset
                except KeyError:
                    sco = "Unset"
                if sco is False:
                    subput += " (bad scope)"
                    color = F"<span class='{color_bad}'>"
                    if summary[key] != "":
                        summary[key] += "+ "
                    summary[key] += "Bad Scope "

                output += f"{color}{name:{width}s}{subput}<br/>"
        output += f"|{last_update}"
        output += "|\n" # last entry in row

    product_summary = True
    for key in summary.values():
        if key != "":
            product_summary = False

    if product_summary:
        output += f"||\n|<span class='{color_good}'><b>{product}</b> (Summary)"
    else:
        output += f"||\n|<span class='{color_bad}'><b>{product}</b> (Summary)"
    for key in summary:
        #  logger.info(F"key: {key} -- {value}")
        if summary[key] == "":
            output += f"|<span class='{color_good}'>OK"
        else:
            if color_bad:
                output += f"|<span class='{color_bad}'>{summary[key]}"
            else:
                output += "|Missing"
    output += f"|{last_update}"
    output += "|\n"
    return output

def refresh_visualisation_from_cache(cache, attributes_to_check):
    """Refresh the output with current data from cache"""
    verdict = jsontools.get_all_verdicts(cache)
    output = ""
    output += "# naco - the **N**FDI **A**ttribute **CO**nformity Checker\n\n"
    output += "Listed are **attribute categories** (e.g. Identifier) and the actual attributes "
    output += "that belong into these attribute categories.\n "
    output += "Please check the [EOSC AAI Architecture 2022](https://docs.google.com/document/d/1L56uUvs9Tap1tlanendFm-_Jzfu6L7i7FYvzvsX1Mfk/edit#heading=h.oc8k4gs9408w) and the "
    output += "[NFDI Attribute Profile](https://doc.nfdi-aai.de/attributes/) for more "
    output += "information.\n\n"

    for attr_name in attributes_to_check.keys():
        do_dashes = True
        attrs_to_check = attributes_to_check[attr_name]
        output += f"{attr_name}\n\n"
        for oa_conf in verdict:
            OP_name = CONFIG.get("ops", oa_conf, fallback=oa_conf)
            logger.debug(f"rendering output for {oa_conf} - {attr_name}")
            #  output += f"| {OP_name}|"
            #  output += F"Last update: {verdict[oa_conf]['last update']}|\n\n"
            output += render_output_header(
                attrs_to_check, color="bg_gray", dashes=do_dashes
            )
            do_dashes = False

            color_bad = None
            if attr_name == "## Mandatory Attributes":
                color_bad = "bg_red"
            output += render_output(
                verdict[oa_conf][attr_name],
                f"{OP_name}",
                color_bad=color_bad,
                last_update=verdict[oa_conf]["last update"],
            )
        output += "\n"
    md2html(output)
