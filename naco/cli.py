"""Console script for naco."""
# vim:foldmethod=indent
# pylint: disable = logging-fstring-interpolation,
import logging
import sys

from naco.logsetup import logger
from naco.config import CONFIG
from naco.parse_args import args
from naco import jsontools, tokentools, spec_checker, output_tools, diskcache
from naco.exceptions import MyException

# logger = logging.getLogger(__name__)


def main():
    """Console script for naco."""

    cache = diskcache.get_cache()

    if args.cache:
        print(f"{'Product':14s}|       TTL")
        print("--------------+-----------")
        for key in cache.keys(tag="verdict"):
            obj = cache.get(key, tag="verdict")
            try:
                print(f"{key:14s}|{cache.ttl(key, 'verdict'):9.0f}s")
            except TypeError:
                print(f"{key:14s}|      permanent")
        return 0
    elif args.cachedel:
        try:
            cache.delete(args.cachedel, tag="verdict")
            logger.info(f"Deleted {args.cachedel} from cache")
        except Exception:
            raise
        return 1

    mandatory_info = jsontools.load_json(
        CONFIG["main"]["mandatory_attributes_access_token"]
    )
    optional_info = jsontools.load_json(
        CONFIG["main"]["optional_attributes_access_token"]
    )
    attributes_to_check = {
        "## Mandatory Attributes": mandatory_info,
        "## Optional Attributes": optional_info,
    }
    #  attributes_to_check = {
        #  "## Mandatory Attributes": mandatory_info,
    #  }

    if args.op:
        op = args.op
        op_name = CONFIG["ops"][op]
        logger.info(f"op: {op} -- {op_name}")
        ops = [(op)]
    else:
        ops = CONFIG["ops"].keys()
    if args.update:
        ops = []

    ops = list(ops)

    # gather op info
    op_info = {}
    invalid_ops = []
    for oa_conf in ops:
        OP_name = CONFIG.get("ops", oa_conf)
        logger.info(f"getting info from {OP_name}")
        try:
            op_info[oa_conf] = tokentools.get_all_info_by_oa_conf(oa_conf)
        except MyException:
            logger.warning(f"{oa_conf}: Removing from ops")
            # can't modify the list over which we currently loop...
            invalid_ops.append(oa_conf)
    for oa_conf in invalid_ops:
        ops.remove(oa_conf)

    #  for oa_conf in ops:
    #      logger.info(f"OP: {oa_conf}")

    # create verdicts
    verdict = {}
    for oa_conf in ops:
        verdict[oa_conf] = {}
        #  logger.info(f"Creating verdict for '{oa_conf}'")
        OP_name = CONFIG.get("ops", oa_conf)
        logger.info(F"Appending checks for {OP_name}")
        for attr_name, attrs_to_check in attributes_to_check.items():
            logger.info(F"attr_name: {attr_name}")
            checks=[]
            try:
                checks.append((op_info[oa_conf]["user_info"], "User Info"))
            except KeyError:
                logger.warning(F"{oa_conf}: Failed to find user_info: >{jsontools.format_json(op_info[oa_conf])}<")
            try:
                if op_info[oa_conf]["access_token_info"] is not None:
                    checks.append((op_info[oa_conf]["access_token_info"]["body"], "Access Token"))
            except KeyError:
                pass
            try:
                checks.append((op_info[oa_conf]["introspection_info"], "Introspection"))
            except KeyError:
                logger.error(F"{oa_conf}: no introspection info")
            verdict[oa_conf][attr_name] = {}
            for data, check_name in checks:
                #  logger.info('=============================')
                #  logger.info(F"Check name: {check_name}")
                if data is not None:
                    verdict[oa_conf][attr_name][check_name] = spec_checker.check_dict(
                        attrs_to_check, data, check_name, oa_conf
                    )
                    #  logger.info(jsontools.format_json(verdict[oa_conf]))
            jsontools.save_verdict(cache, oa_conf, verdict[oa_conf])
            jsontools.save_json_in_cache(cache, oa_conf, op_info[oa_conf])
        #  jsontools.print_json(verdict[oa_conf])

    # refresh visualisation with updated verdict in cache
    output_tools.refresh_visualisation_from_cache(cache, attributes_to_check)


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
