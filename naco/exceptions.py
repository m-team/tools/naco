"""json tools for naco."""
# vim:foldmethod=indent
# pylint: disable = logging-fstring-interpolation,

import logging

from naco.logsetup import logger
# from naco import logsetup
from naco.config import CONFIG

# logger = logging.getLogger(__name__)

class MyException(Exception):
    name = "Error"
    status_code = 500

    def __init__(self, message, code=500):
        logger.warning(message)
    def render(self) -> dict:
        data = {
            "error": self.name,
            "error_description": str(self),
        }
        return data
