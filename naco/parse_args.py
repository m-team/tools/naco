# vim: tw=100 foldmethod=indent
"""Parse commandline options"""

import argparse
import os
import sys


def parseOptions():
    """Parse commandline options"""

    folder_of_executable = os.path.split(sys.argv[0])[0]
    basename = os.path.basename(sys.argv[0]).rstrip('.py')
    dirname  = os.path.dirname(__file__)

    config_file = os.environ['HOME']+F'/.config/{basename}.conf'
    log_file    = F"{dirname}/{basename}.log"
    log_file    = "cli.log"
    #  log_file = None
    #  log_file    = ""

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--verbose",    "-v",   action="count", default=0, help="Verbosity")
    parser.add_argument("--debug",      "-d",   action="count", default=0, help="Logmode debug")
    parser.add_argument("--config",     "-c",   default=config_file, help="config file")
    parser.add_argument("--logfile",            default=None, help="logfile")
    parser.add_argument("--loglevel",           default="WARNING")
    parser.add_argument("--op",                 default=None, help="OP to run for")
    parser.add_argument("--update",             default=None, help="Only update", action="store_true")
    parser.add_argument("--cache",              default=None, help="Show cache", action="store_true")
    parser.add_argument("--cachedel",           default=None, help="Delete entry from cache", )


    #  print("naco - Nfdi Argument Checker for Oidc")
    return parser


# reparse args on import, unless pytest
if "_pytest" in sys.modules:
    args = {}
elif "uvicorn" in sys.modules:
    # print("ignoring args for uvicorn")
    args = {}
else:
    # print("args parsed")
    args = parseOptions().parse_args()
    # print("WARNING: not parsing args, to support pytest")
