.. _api:

NFDI Attribute Checker OIDC API
===============================

Here is the documentation of the most important modules of **naco**:


naco
----
.. automodule:: naco
   :members:


naco.naco
---------
.. automodule:: naco.naco
   :members:
