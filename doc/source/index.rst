===========================
NFDI Attribute Checker OIDC
===========================

Check Attributes for compliance with NFDI expectations

Documentation
-------------

.. toctree::
   :maxdepth: 1

   naco/installation
   naco/development


Reporting Bugs
--------------

Issues are managed at `github <https://github.com/marcvs/naco/issues/>`_.



API Reference
-------------

If you are looking for information on a specific function, class or
method, this part of the documentation is for you.

.. toctree::
   :maxdepth: 2

   naco/api/index

-----------------


Indices
=======

* :ref:`genindex`
* :ref:`modindex`
